#include <dirent.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>

// defines maximum results to store and length of each result respectivly
#define MAX_RESULTS 256
#define MAX_PATH_SIZE 2048

// constants for the root, current, and parent directories
const char* const rootDir = "/";
const char* const curDir = ".";
const char* const parDir = "..";
// options struct used by `getopts_long_only
const struct option longopts[] = {
	{"name",   required_argument, 0, 'n'},
	{"mmin",   required_argument, 0, 'm'},
	{"inum",   required_argument, 0, 'i'},
	{"exec",   required_argument, 0, 'e'},
	{"delete", no_argument,       0, 'd'},
	{0, 0, 0, 0}
};

// current index into the results array
int idx = 0;
// results array to store paths
char* results[MAX_RESULTS * sizeof(char*)];

void find(char* dir, char** vals);

int main(int argc, char** argv)
{
	// argument array thats passed to `find`
	char* args[5] = {0};
	// getopt_long_only result
	int c;
	// call getopt function passing in long options from above
	while ((c = getopt_long_only(argc, argv, "", longopts, NULL)) != -1) {
		// check on which option it received
		// optarg is the pointer to the argument used for the option
		switch (c) {
			case 'n':
				args[0] = optarg;
				break;
			case 'm':
				args[1] = optarg;
				break;
			case 'i':
				args[2] = optarg;
				break;
			case 'e':
				puts("exec flag not implemented");
				exit(1);
			case 'd':
				args[4] = "";
				break;
			default:
				exit(1);
		}
	}

	// starting location to search
	char* loc;
	// if only one arg, no location given,
	// therefore start at current directory: `.`
	if (argc == 1) {
		loc = calloc(2, 1);
		loc[0] = '.';
	}
	else {
		// after getopt block, the first argument is now 
		// at argc - 1 position in argv. copy it into loc
		loc = strdup(argv[argc - 1]);
		int len = strlen(loc);
		// remove trailing `/` if needed
		if (loc[len - 1] == '/') {
			loc[len - 1] = 0;
		}
	}
	// store starting location at index 0, increment idx
	results[idx++] = loc;
	// call find method
	find(loc, args);

	// printing the results:
	// if no criteria given, also print starting location
	// else start at first result found in `find` method
	int i = argc < 3 ? 0 : 1;
	// print out results
	for (; i < idx; i++) {
	    printf("%s\n", results[i]);
	    free(results[i]);
	    results[i] = NULL;
	}
	if (results[0]) free(results[0]);

	return 0;
}

// dir = path
// vals = criteria args
void find(char* dir, char** vals)
{
	// pointer to entry in the directory
	struct dirent* entry;

    // pointer to opened directory
	DIR* dirfile = opendir(dir);
	// check if able to open the directory `dir`
	if (dirfile != NULL) {
		// read the next entry in the directory
		// do this until NULL, which means no more entries
		while ((entry = readdir(dirfile)) != NULL) {
			// pointer to entry's name
			const char* temp = entry->d_name;
			// check if entry read was the current or parent dir
			// avoids repeatedly opening current directory
			if (strcmp(temp, curDir) != 0 && strcmp(temp, parDir) != 0) {
				// allocate space for full path string
				char* fullPath = malloc(MAX_PATH_SIZE);
				// copy directory name we are currently in to fullPath
				strcpy(fullPath, dir);
				// add `/`
				strcat(fullPath, rootDir);
				// add entry name
				strcat(fullPath, temp);

				// attempt to open fullPath as directory
				DIR* subsubdp = opendir(fullPath);
				// if not null, entry was directory
				if (subsubdp != NULL) {
					// close directory, call `find` with fullPath
					closedir(subsubdp);
					find(fullPath, vals);
				}
				// if null, entry was a file

				// call `stat` on the file and store result
				struct stat fstats;
				stat(fullPath, &fstats);

				// criteria checks: if no match, goto no_match
				// else, continue as criteria is met
				// name criteria check
				if (vals[0] != NULL) {
					// if name different than entry name
					if (strcmp(temp, vals[0]) != 0)
						goto no_match;
				}
				// mmin criteria check
				// n: exactly n mins
				// -n: less than n mins
				// +n: more than n mins
				if (vals[1] != NULL) {
					// convert arg to long, mul by 60 for secs
					time_t t = atol(vals[1]) * 60;
					// files modification time from stat, in secs
					time_t modtime = fstats.st_mtime;
					// current time in secs
					time_t now = time(NULL);
					// switch over first char, see above for meaning
					switch (vals[1][0]) {
						case '-':
							if ((now - modtime) > -t)
								goto no_match;
							break;
						case '+':
							if ((now - modtime) < t)
								goto no_match;
							break;
						default:
							if (now - modtime - t >= 0)
								goto no_match;
					}
				}
				// inum criteria check
				if (vals[2] != NULL) {
					// convert arg to long, type of ino_t
					ino_t inum = atol(vals[2]);
					// compare against files inode
					if (fstats.st_ino != inum)
						goto no_match;
				}
				// if here, all criteria are satisfied
				// delete check
				if (vals[4] != NULL) {
					//call remove on the file
					remove(fullPath);
					// remove doesnt give any output, so just continue loop
					continue;
				}
				// current path is a valid result
				// store pointer to it in results, increment idx
				results[idx++] = fullPath;
				continue;
				// if any of the above checks failed,
				// current path isnt valid result, free its memory
			no_match:
				free(fullPath);
			}
		}
		// close directory
		closedir(dirfile);
	}
	// directory passed not valid
	else {
		// print error message and exit program
		printf("ERROR: Cant open directory: %s\n", dir);
		exit(2);
	}
}
