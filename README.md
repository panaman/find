# find
## Project for Operating Systems class
### Minimal reproduction of linux `find` command

How to run program:
1. run `make`. This will build the binary `find` in the current directory
2. run `make test`. This will create text files with output of the requirements
3. observe contents of text files `req{1,2a,2b,2c}` for output
4. run `./find .` with any criteria and the `-delete` flag to test deletion
5. verify file you specified is deleted
6. run `./find` with any other criteria you wish to test

