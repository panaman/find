BIN=find
FILE=test6
DIR=testdir/dir1/dir2/$(FILE)
INUM=$(shell ls -i ./$(DIR) | cut -d ' ' -f 1)

all: main.c
	clang main.c -o $(BIN)

test: $(BIN)
	echo "Command: ./find ." > req1
	./$^ . >> req1
	echo "Command: ./find . -name $(FILE)" > req2a
	./$^ . -name $(FILE) >> req2a
	echo "Command: ./find . -mmin -5" > req2b
	./$^ . -mmin -5 >> req2b
	echo "Command: ./find . -inum $(INUM)" > req2c
	./$^ . -inum $(INUM) >> req2c

clean: find req1 req2a req2b req2c
	rm $^
